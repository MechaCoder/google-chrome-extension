// console.log("plugin");
var storage = chrome.storage.local;
var strutVal = "mp_title_mod";

//domInput( stringCreate() )

function stringCreate( _SeverId, _port ) {
  if( _port === undefined ){
    var string = "[ " + _SeverId + " ] "
  } else {
    var string = "[ " + _SeverId + " - " + _port + " ] "
  }
  return string;
}


function domInput( _string ) {
  try {
    $(document).find('title').prepend( _string );
    return true
  } catch (e) {
    console.error(e);
  }
}

var hostname = window.location.hostname.split(".");

var port = window.location.port;
if( port === "" ){
  port = "80"
}

var local_hostname = window.location.hostname.split('.')
storage.get({strutVal: ""}, function(_d){
  var data = JSON.parse( _d.strutVal );

  if( data["json_v"] === null || data["json_v"] === undefined ){ //ver 0
        var foundit = false
        for (var o = 0; o < local_hostname.length; o++) {
          //local_hostname[o]
          for (var i = 0; i < data.length; i++) { if(local_hostname[o] === data[i].def){foundit = true; domInput( stringCreate( data[i].id, port ) ); } }
        }
        if( foundit === false ){
          if( window.location.hostname.length != 0 ){
            if( hostname[0] === "127" || hostname[0] === "local" || hostname[0] === "localhost" ){
              $(document).find("title").prepend( stringCreate("LH", port) );
            } else if ( hostname[0] === "openbd" ) {
              $(document).find("title").prepend( stringCreate("CFML REF", port) );
            } else if (hostname[1] === "w3schools") {
              var path = window.location.pathname.split("/");
              var w3Array = ["html", "css", "js", "sql", "bootstrap", "jquery", "asp", "colors" ]
              for (var i = 0; i < w3Array.length; i++) {
                if ( path[1] === w3Array[i] ) {
                  $(document).find("title").prepend( stringCreate("W3C REF -" + w3Array[i], port) );
                }
              }
            }
      }
    }
  } else if ( data["json_v"] === 1 ){ //ver 1
    //json ver has the intro duction setting
    var settings = data['settings'];
    var urlDefs = data['defs'];
    var urlArray = []
    if( settings["no_defaulting"] != undefined){
      if( settings["no_defaulting"] == true ){
        //run defaults
        urlArray = [ { "id": "LH", "def": "127"}, { "id": "LH", "def": "local"}, { "id": "LH", "def": "localhost"}, { "id": "CFML BD", "def": "localhost"} ];
        //theses are the defaults
        for (var i = 0; i < urlDefs.length; i++) {// the
          urlArray.push( urlDefs )
        }
      }
    }
    if (settings.no_port === "true"){
        port = undefined;
    }

    for (var i = 0; i < data.defs.length; i++) {
      urlArray.push(data.defs[i])
    }

    for (var i = 0; i < urlArray.length; i++){
      for (var o = 0; o < hostname.length; o++){
        if( urlArray[i].def === hostname[o] ){
          domInput(stringCreate( urlArray[i].id, port ));
        }
      }
    }
  }
})//storage.get
