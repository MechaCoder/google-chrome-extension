#page title Modifier.
this plugin will allow you to Modify you page title based on a held json object which is held by the google chrome api localy


##creating object
by clicking on the icon in the top right of a browser you will find the plugin's ui which allows you to enter your custom objects that will detect a part of url and then display this a string that you have defended.

### JSON
all the setting of URL definition and settings is done use json this is so you can save you setting and have different json for different projects if need them, the json is required and in would use the template provided.

``` json
{
  "settings": {
  "no_defaulting": "true",
  "no_port": "true"
  },
  "defs":[

    { "id": "Q&A", "def": "ask"},
    { "id": "work", "def": "myworkURL"},
  ],
  "json_v": 1
}
```
### settings
#### no_defaulting
this is some that have been bundleded to have this setting set to means that you do not want the defaults to be used.
#### no_port
there may be points where you don't want the port to show om the tabs.
#### defs
there are two items that in the defs the id and the defs, id is the string that is added the the tab and the def is the url segment if you where to dubble click on a hostname poce of the hostname that would do it.
### json_v
this is basicly the version of the json current set to '1' but if the structure of the json changes then this number will change.
