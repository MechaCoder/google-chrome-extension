var storage = chrome.storage.local;
var tab     = chrome.tabs
var strutVal = "mp_title_mod";

function loadData( _type ){
  storage.get({strutVal: null},function( _data ){
    $('.codebox').find('pre').find('code').text(_data.strutVal);
    $("button[name='copyObj']").attr('data-clipboard-text', _data.strutVal )
  })
}
loadData()

$( document ).ready(function(){

    // hljs.initHighlightingOnLoad();
    // hljs.configure({});

    $("button[name='edit']").on('click', function( _e ){
        _e.preventDefault();
        var code = $('.codebox').find("pre").find('code').text();
        $('.codebox').find("pre").remove();
        $('.codebox').append("<textarea class='json codeInput hljs'></textarea>" );
        $('.codebox').find('textarea').text(code)
        $('button[name="save"]').removeAttr('disabled');
        $('button[name="reset"]').removeAttr('disabled');
        $('button[name="edit"]').attr('disabled', 'true');
    });

    $("button[name='reset']").on('click', function( _e ){
      _e.preventDefault();
      $('.codebox').find("textarea").val( "{}" )
    })

    // $("button[name='copyObj']").
    var clip = new Clipboard("button[name='copyObj']").on('success',function(){
      sweetAlert({
        title: "Object Copied!",
        text: "your json has been coypied Clipboard",
        type: "info",
        customClass: 'sweetAlertCustom'

      });
    })

    $("button[name='save']").on('click', function( _e ){
      _e.preventDefault();
      var response = $("textarea.codeInput").val();
      try{
        //some vaildation

        JSON.parse(response)
        debugger;
        storage.set({strutVal:response});
        loadData();
        location.reload()
      } catch( _e ) {alert( _e )};
    });

    $(".j_report_bug").on('click', function(_e) {
      _e.preventDefault()
      storage.get({strutVal: null},function( _data ){
          var bugObject = {
            "user_object": JSON.parse(_data.strutVal),
            "the_ver": $('.ver_id').attr('value'),
            "msg": ''
          }

      swal({
        title: "<h5>can you tell me more?</h5>",
        type: "input",
        closeOnConfirm: false,
        customClass: 'sweetAlertCustom',
        html: true
      },
        function( _d ){
          bugObject['msg'] += _d
          debugger;
          swal({
            title: "copy this pls",
            text: "<textarea style='margin: 0px;width: 350px; height: 120px; background-color:#002b36; color:#839496;' > " + JSON.stringify(bugObject) + " </textarea>",
            html: true,
            customClass: 'sweetAlertCustom'
          },function(){
            chrome.tabs.create({'url': 'https://bitbucket.org/MechaCoder/google-chrome-extension/issues/new' });
          });
        });
          // debugger;
        });// storage.get
      });
})
